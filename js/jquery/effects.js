$(document).ready(() => {


    var letras = [".",".","."];
    var angulo = 0;
    setInterval(function () {
      angulo += 1;
  
      $("#cargando img").css("transform", `rotate(${angulo}deg)`);
      $("#error img").css("transform", `rotate(${angulo}deg)`);
    }, 20);
  
    var cont = 0;
    setInterval(function () {
      $("#cargando h1").text($("#cargando h1").text() + letras[cont]);
  
      if (cont == letras.length) {
        $("#cargando h1").text("cargando");
        cont = 0;
      } else {
        cont++;
      }
    }, 150);
  });
  