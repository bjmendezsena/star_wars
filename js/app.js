"use strict";

const url = "https://swapi.dev/api/";

let starWarsLinks;
cargarDatos();

/**
 * Carga los datos Iniciales de los links.
 *
 * Si la promesa da respuesta oculta el mensaje de error y muestra los botones y el contenido de ellos.
 * si falla, oculta los botones y el contenido, y muestra el mensaje de error
 */
function cargarDatos() {
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      document.getElementById("cargando").style.display = "none";
      document.getElementById("container").style.display = "block";
      document.getElementById("contenido").style.display = "block";
      starWarsLinks = data;
    })
    .catch((err) => {
      showErrorMessage();
    });
}

/**
 * Muestra y carga los datos a un div dependiendo del botón pulsado.
 * @param {*} divId id del div a mostrar
 */
function activar(divId) {
  clearDivs();
  mostrarDiv(divId);
  switch (divId) {
    case "pla":
      showSelected("#" + divId + " #planets");
      break;
    case "per":
      showSelected("#" + divId + " #people");
      break;
    case "pel":
      showSelected("#" + divId + " #films");
      break;
    case "nav":
      showSelected("#" + divId + " #starships");
      break;
    case "veh":
      showSelected("#" + divId + " #vehicles");
      break;
    case "species":
      listBy(starWarsLinks.species, "name");
      break;
  }
}

/**
 * Muestra el div dependiendo del botón pulsado
 * @param {*} divName
 */
function mostrarDiv(divName) {
  var div = document.getElementById(divName);
  div.style.display = "block";
}

/**
 * Oculta todos los divs a mostrar
 */
function clearDivs() {
  document.getElementById("pla").style.display = "none";
  document.getElementById("per").style.display = "none";
  document.getElementById("pel").style.display = "none";
  document.getElementById("nav").style.display = "none";
  document.getElementById("veh").style.display = "none";
  document.getElementById("species").style.display = "none";
}

/**
 * Hace una petición Ajax a una api
 * @param {*} link link de la api
 * @param {*} orderby atributo como referencia para ordenar
 */
function listBy(link, orderby) {
  fetch(link)
    .then((response) => response.json())
    .then((data) => {
      let result = data.results;


      //characters
      var name = link.split("api/")[1].split("/")[0];

      //Primero verifico si tengo que ordenar por numero de episodios y lo ordeno
      if (orderby == "characters") {
        var resultEpisodes = result
          .sort(function (a, b) {

            if (parseInt(a[orderby].length) > parseInt(b[orderby].length)) return 1;
            if (parseInt(a[orderby].length) < parseInt(b[orderby].length)) return -1;
            return 0;
          });

          updateData(resultEpisodes, name); //Recojo solo los elementos que no son números y los ordeno
      } else {//Si no es por numero de episodios

        var resultNumbers = result
          .filter((item) => !isNaN(item[orderby]))
          .sort(function (a, b) {
            if (parseInt(a[orderby]) > parseInt(b[orderby])) return 1;
            if (parseInt(a[orderby]) < parseInt(b[orderby])) return -1;
            return 0;
          }); //Recojo solo los elementos que son números y los ordeno

        var resultCharacters = result
          .filter((item) => isNaN(item[orderby]))
          .sort(function (a, b) {
            if (a[orderby] > b[orderby]) return 1;
            if (a[orderby] < b[orderby]) return -1;
            return 0;
          }); //Recojo solo los elementos que no son números y los ordeno


        //verifico si el array de números recogió datos
        if (resultNumbers.length > 0) {
          var finalResult = [];
          //Verifico si además he recogido datos que no son números
          if (resultCharacters.length > 0) {
            //Si tengo números y caracteres no numericos los no numericos los añado al final
            finalResult = [...resultNumbers, ...resultCharacters];
          }else{
            finalResult = [...resultNumbers];
          }

          updateData(finalResult, name); //Muestro los números
        } else if (resultNumbers.length == 0) {
          //Si el array que recoge numeros está vacío muestro solo los resultados
          //De tipo carácter
          updateData(resultCharacters, name);
        }
      }
    })
    .catch((err) => {
      document.getElementById("error").style.display = "block";
    });
}

/**
 * Recogemos la opción seleccionada dependiendo del botón clickado
 * @param {*} querySelector ids de los elementos seleccionados
 */
function showSelected(querySelector) {
  //Le pasamos el nombre del div y el id de la etiqueta Select que a la vez
  //Es el mismo nombre del campo llave de los primeros datos que hemos recogido de la api
  var combo = document.querySelector(querySelector);
  var selectedoption = combo.options[combo.selectedIndex].value;

  //Hacemos un split para dividir el id del dib y del selector
  var link = querySelector
    .split(" ")[1]
    .substring(1, querySelector.split(" ")[1].lenght);

  listBy(starWarsLinks[link], selectedoption);
}

/**
 * Añade cada elemento del array al dom dependiendo del tipo de dato
 * @param {*} data dato a añadir
 * @param {*} type tipo de dato
 */
function updateData(data, type) {
  switch (type) {
    case "people":
      putPeopleData(data);
      break;
    case "planets":
      putPlanetsData(data);
      break;
    case "films":
      putFilmsData(data);
      break;
    case "starships":
      putNavesData(data);
      break;
    case "vehicles":
      putVehiclesData(data);
      break;
    case "species":
      putSpeciesData(data);
      break;
  }
}

/**
 * Añade los dayos de los personajes
 * @param {*} data datos a añadir
 */
function putPeopleData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Nombre",
    "Fecha de nacimiento",
    "Género",
    "Altura",
    "Masa muscular",
    "Color de piel",
    "Color de ojos"
  );

  //Agregamos los datos a la taba
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.name,
        item.birth_year,
        item.gender,
        item.height + " cm",
        item.mass + " kg",
        item.skin_color,
        item.hair_color
      );
    }
  }
}

/**
 * Añade los datos de los vehículos
 * @param {*} data datos a añadir
 */
function putVehiclesData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Nombre",
    "Clase",
    "Modelo",
    "Capacidad de cargo",
    "Distribuidor",
    "Máxima velocidad atmosférica",
    "Fecha de creación"
  );

  //Agregamos los datos a la tabla
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.name,
        item.vehicle_class,
        item.model,
        item.cargo_capacity + " Kg",
        item.manufacturer,
        item.max_atmosphering_speed + " Km",
        item.created
      );
    }
  }
}

/**
 * Añade los dayos de las especies
 * @param {*} data datos a añadir
 */
function putSpeciesData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Nombre",
    "Idioma",
    "Altura media",
    "Promedio de vida",
    "Clasificación",
    "Designación",
    "Color de ojos",
    "Color de piel",
    "Color de pelo"
  );

  //Agregamos los datos a la tabla
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.name,
        item.language,
        item.average_height == "n/a"
          ? item.average_height
          : item.average_height + " cm",
        item.average_lifespan == "unknown" ||
          item.average_lifespan == "indefinite"
          ? item.average_lifespan
          : item.average_lifespan + " cm",
        item.classification,
        item.designation,
        item.eye_colors,
        item.skin_colors,
        item.hair_colors
      );
    }
  }
}

/**
 * Añade los dayos de las naves
 * @param {*} data datos a añadir
 */
function putNavesData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Nombre",
    "Modelo",
    "Clase",
    "Distribuidor",
    "Coste",
    "Máxima velocidad en atmósfera",
    "Pasajeros"
  );

  //Agregamos los datos a la tabla
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.name,
        item.model,
        item.starship_class,
        item.manufacturer,
        item.cost_in_credits == "unknown"
          ? item.cost_in_credits
          : item.cost_in_credits + " $",
        item.max_atmosphering_speed == "n/a"
          ? item.max_atmosphering_speed
          : item.max_atmosphering_speed + " km",
        item.passengers
      );
    }
  }
}

/**
 * Añade los dayos de los planetas
 * @param {*} data datos a añadir
 */
function putPlanetsData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Nombre",
    "Clima",
    "Diámetro",
    "Período de rotación",
    "Superfície del agua",
    "Terreno",
    "Población"
  );

  //Agregamos los elementos al body de la tabla
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.name,
        item.climate,
        item.diameter + " km",
        item.rotation_period + " h",
        item.surface_water == "unknown"
          ? item.surface_water
          : item.surface_water + "%",
        item.terrain,
        item.population == "unknown"
          ? item.population
          : item.population + " hab."
      );
    }
  }
}

/**
 * Añade los dayos de las películas
 * @param {*} data datos a añadir
 */
function putFilmsData(data) {
  clearTable();

  //Agregamos cabeceras
  agregarCabecera(
    "Título",
    "Episodios",
    "Director",
    "Fecha de creación",
    "Productor"
  );

  //Agregamos los datos a la tabla
  for (var item of data) {
    if (item) {
      putTrOnTheBody(
        item.title,
        item.characters.length,
        item.director,
        item.created,
        item.producer
      );
    }
  }
}

/**
 * Muestra solo el error y oculta los demás elementos
 */
function showErrorMessage() {
  clearDivs(); //Ocultamos los divs
  document.getElementById("cargando").style.display = "none";
  document.getElementById("container").style.display = "none";
  document.getElementById("content").style.display = "none";
  document.getElementById("error").style.display = "block";
}

/**
 * Agrega una cabecera a una tabla
 * @param {*} text Nombre de la cabecera
 */
function agregarCabecera(...text) {
  var cabecera = document.getElementById("cabecera");
  for (var txt of text) {
    var th = document.createElement("th");
    var h3 = document.createElement("h3");

    h3.appendChild(document.createTextNode(txt));
    th.appendChild(h3);
    cabecera.appendChild(th);
  }
}

/**
 * Limpia el contenido de la cabecera
 */
function clearTable() {
  var cabecera = document.getElementById("cabecera");
  var body = document.getElementById("res");
  cabecera.innerHTML = "";
  body.innerHTML = "";
}

/**
 * Añade un tr con datos en el cuerpo de la tabla
 * @param  {...any} text uno o varios datos para añadir
 */
function putTrOnTheBody(...data) {
  var body = document.getElementById("res");
  var tr = document.createElement("tr");

  for (var text of data) {
    var th = document.createElement("th");
    th.appendChild(document.createTextNode(text));
    tr.appendChild(th);
  }

  body.appendChild(tr);
}
